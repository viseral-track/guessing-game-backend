package com.shack.guessinggame.controller;

import com.shack.guessinggame.datamodel.APIResponse;
import com.shack.guessinggame.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public Mono<ResponseEntity<APIResponse>> loginAPI(@RequestParam(name = "username")String username){
        return userService.login(username)
                .flatMap(s -> Mono.just(ok(s)));
    }

    @GetMapping("/create")
    public Mono<ResponseEntity<APIResponse>> createAPI(@RequestParam(name = "username", required = false)String username, @RequestParam(name = "randomize", defaultValue = "false")boolean randomize){
        return userService.create(username, randomize)
                .flatMap(s -> Mono.just(ok(s)));
    }

}
