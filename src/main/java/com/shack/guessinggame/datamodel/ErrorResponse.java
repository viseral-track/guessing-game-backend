package com.shack.guessinggame.datamodel;

import com.shack.library.common.EnumBase;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse {

    private EnumBase.ERRORCODE errorCode;
    private String errorDesc;
}
