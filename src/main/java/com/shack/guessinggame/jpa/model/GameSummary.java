package com.shack.guessinggame.jpa.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tbl_game_summary")
public class GameSummary {

    @Id
    @Column(name = "summary_id", length = 64)
    private String summaryId;

    @Column(name = "game_start_ts", columnDefinition = "TIMESTAMP")
    private LocalDateTime gameStartTimestamp;

    @Column(name = "game_end_ts", columnDefinition = "TIMESTAMP")
    private LocalDateTime gameEndTimestamp;

    @Column(name = "attempts",length = 3)
    private Integer attempts;

    @Column(name = "winning_value", length = 1)
    private Integer winningValue;
}
