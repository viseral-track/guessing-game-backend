package com.shack.guessinggame.jpa.repo;

import com.shack.guessinggame.jpa.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepo extends JpaRepository<Game, Integer> {
}
