package com.shack.guessinggame.datamodel;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GuessSummary {

    private int gameId;
    private boolean guessResult;
    private int winningValue;
    private int guess;
    private String feedback;

}
