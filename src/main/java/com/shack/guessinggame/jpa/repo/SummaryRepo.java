package com.shack.guessinggame.jpa.repo;

import com.shack.guessinggame.jpa.model.GameSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SummaryRepo extends JpaRepository<GameSummary, String> {
}
