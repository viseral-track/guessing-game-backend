package com.shack.guessinggame.service;

import com.github.javafaker.Faker;
import com.shack.guessinggame.datamodel.APIResponse;
import com.shack.guessinggame.datamodel.ErrorResponse;
import com.shack.guessinggame.jpa.model.User;
import com.shack.guessinggame.jpa.repo.UserRepo;
import com.shack.library.aop.interfaces.ExecutionTimeAnalysis;
import com.shack.library.aop.interfaces.SendDelta;
import com.shack.library.common.EnumBase;
import com.shack.library.common.datamodel.SyncModel;
import com.shack.library.service.DeltaLogComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@Service
public class UserService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private GameService gameService;

    @Autowired
    private DeltaLogComponent deltaLogComponent;


    @ExecutionTimeAnalysis
    public Mono<APIResponse> login(String username) {
        return Mono.just(userRepo.findDistinctByUsername(username))
                .filter(s -> s.isPresent())
                .map(s -> s.get())
                .flatMap(s -> {
                    s.setLastAccessedTimestamp(LocalDateTime.now());
                    return Mono.just(new APIResponse(userRepo.save(s)));
                })
                .doOnSuccess(s -> {
                    gameService.createGameIfNoExisting();
                    deltaLogComponent.sendDelta(new SyncModel(EnumBase.GROUP.USER, EnumBase.ACTION.GUESSATTEMPT, username, EnumBase.RESULT.SUCCESS, null, null));
                })
                .switchIfEmpty(Mono.just(new APIResponse(new ErrorResponse(EnumBase.ERRORCODE.SHACK_400001, EnumBase.ERRORCODE.SHACK_400001.getErrorDesc()))));
    }


    @ExecutionTimeAnalysis
    public Mono<APIResponse> create(String username, Boolean randomize) {
        return Mono.just("")
                .flatMap(s -> {
                    String name = username != null ? username : null;
                    Boolean random = randomize != null ? randomize : false;
                    User user;
                    if (random) {
                        user = new User(new Faker().name().username(), LocalDateTime.now(), LocalDateTime.now());
                    } else {
                        user = new User(name, LocalDateTime.now(), LocalDateTime.now());
                    }
                    return Mono.just(user);
                })
                .flatMap(s -> Mono.just(new APIResponse(userRepo.save(s))));
    }
}
