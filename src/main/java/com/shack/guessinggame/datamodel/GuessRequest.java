package com.shack.guessinggame.datamodel;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class GuessRequest {

    private int gameId;
    private int guess;

}
