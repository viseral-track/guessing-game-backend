package com.shack.guessinggame.jpa.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tbl_user")
public class User {

    @Id
    @Column(name = "username", length = 64)
    private String username;

    @Column(name = "created_ts", columnDefinition = "TIMESTAMP")
    private LocalDateTime createdTimestamp;

    @Column(name = "last_accessed_ts", columnDefinition = "TIMESTAMP")
    private LocalDateTime lastAccessedTimestamp;

}
