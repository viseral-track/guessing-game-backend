package com.shack.guessinggame.datamodel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class APIResponse {

    private LocalDateTime responseTimestamp;
    private Object data;

    public APIResponse(Object data) {
        this.responseTimestamp = LocalDateTime.now();
        this.data = data;
    }
}
