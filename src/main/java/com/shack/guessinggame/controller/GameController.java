package com.shack.guessinggame.controller;

import com.shack.guessinggame.datamodel.GuessRequest;
import com.shack.guessinggame.jpa.repo.GameRepo;
import com.shack.guessinggame.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/game")
public class GameController {

    @Autowired
    private GameService gameService;

    @PostMapping("/guess")
    public Mono<ResponseEntity> guessAPI(@RequestBody GuessRequest request){
        return gameService.guess(request.getGameId(), request.getGuess())
                .flatMap(s -> Mono.just(ok(s)));
    }

    @GetMapping("/gameList")
    public Mono<ResponseEntity> gameListAPI(){
        return gameService.getGameList()
                .flatMap(s -> Mono.just(ok(s)));
    }
}
