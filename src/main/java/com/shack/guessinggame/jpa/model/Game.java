package com.shack.guessinggame.jpa.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@Table(name = "tbl_game")
public class Game {

    @Id
    @GeneratedValue
    private Integer gameId;
    @Column(name = "created_ts", columnDefinition = "TIMESTAMP")
    private LocalDateTime createdTs;
    private int winningValue;

    public Game(int winningValue) {
        this.createdTs = LocalDateTime.now();
        this.winningValue = winningValue;
    }
}
