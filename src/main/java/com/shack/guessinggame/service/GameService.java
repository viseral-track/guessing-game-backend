package com.shack.guessinggame.service;

import com.shack.guessinggame.datamodel.APIResponse;

import com.shack.guessinggame.datamodel.ErrorResponse;
import com.shack.guessinggame.datamodel.GuessSummary;
import com.shack.guessinggame.jpa.model.Game;
import com.shack.guessinggame.jpa.model.GameSummary;
import com.shack.guessinggame.jpa.repo.GameRepo;
import com.shack.guessinggame.jpa.repo.SummaryRepo;
import com.shack.library.aop.interfaces.ExecutionTimeAnalysis;
import com.shack.library.common.EnumBase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Service
@Slf4j
public class GameService {

    @Autowired
    private GameRepo gameRepo;

    @Autowired
    private SummaryRepo summaryRepo;



    @ExecutionTimeAnalysis
    public Mono<APIResponse> guess(int gameid, int guess){
        return Mono.just(gameRepo.findById(gameid))
                .filter(s -> s.isPresent())
                .map(s -> s.get())
                .flatMap(s -> {
                    if(s.getWinningValue() == guess){

                        GameSummary summary = new GameSummary(UUID.randomUUID().toString(), s.getCreatedTs(), LocalDateTime.now(), 0, s.getWinningValue());
                        summaryRepo.save(summary);
                        gameRepo.deleteById(gameid);
                        return Mono.just(new GuessSummary(s.getGameId(), true, s.getWinningValue(), guess, "You chose wisely."));
                    }
                    else {
                        return Mono.just(new GuessSummary(s.getGameId(), false, 0, guess, "You chose poorly. Try again."));
                    }
                })
                .map(s -> new APIResponse(s))
                .switchIfEmpty(Mono.just(new APIResponse(new ErrorResponse(EnumBase.ERRORCODE.SHACK_400002, EnumBase.ERRORCODE.SHACK_400002.getErrorDesc()))));
    }

    public void createNewGame(){
        Mono.just(new Game(new Random().nextInt(10)+1))
                .flatMap(s -> Mono.just(gameRepo.save(s)))
                .subscribe(s -> log.info("Game created: {}", s.toString()));
    }

    public void createGameIfNoExisting(){
        Mono.just(gameRepo.findAll())
                .filter(games -> games.size() <= 0)
                .flatMap(s -> Mono.just(gameRepo.save(new Game(new Random().nextInt(10)+1))))
                .subscribe(s -> log.info("Game created since no preset game exists: {}", s.toString()));
    }

    public Mono<APIResponse> getGameList(){
        return Mono.just(gameRepo.findAll())
                .filter(s -> s.size() > 0)
                .map(s -> {
                    List<Integer> gameList = new ArrayList<>();
                    s.forEach(r -> gameList.add(r.getGameId()));
                    return gameList;
                })
                .flatMap(s -> Mono.just(new APIResponse(s)))
                .switchIfEmpty(Mono.just(new APIResponse(new ErrorResponse(EnumBase.ERRORCODE.SHACK_200001, EnumBase.ERRORCODE.SHACK_200001.getErrorDesc()))));
    }


}
